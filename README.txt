INTRODUCTION
------------

Alter webform email text depending on the options selected during submission.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/sandbox/jphuxley/2853952

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2853952


REQUIREMENTS
------------

This module requires the following modules:

 * Webform (https://drupal.org/project/webform)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

NB: Webform Email Triggers only works with multi value components such as drop
downs and radio buttons. You must add at least one of these components to
your webform for the module to work.

 * Navigate to the webform you want to edit click Webform > E-mails.

 * Add or edit an existing email.

 * Under 'Triggers' you will find an area for all multi select questions in your
   form, and a text box for each option.

 * For each option, enter the text you would like to appear in the email should
   the user select this option. If the user selects multiple options, they will
   all be shown in the email.

 * Underneath the text box you will find a token. Copy and paste this in to the
   body of your email where you would like the text to appear. Repeat this step
   for all options. Alternatively you can copy the component token to display
   all values chosen.

 * Save the email, and upon completing a form the email with replaced tokens
   should be sent.

MAINTAINERS
-----------

Current maintainers:
 * John Huxley (JPHuxley) - https://www.drupal.org/u/jphuxley
